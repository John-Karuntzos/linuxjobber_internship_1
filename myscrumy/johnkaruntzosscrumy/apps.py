from django.apps import AppConfig


class JohnkaruntzosscrumyConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'johnkaruntzosscrumy'
