
from django.urls import path
from django.urls import include
from . import views

app_name = 'johnkaruntzosscrumy'

urlpatterns = [
	path('', views.index,name='about'),
	path('accounts/',include('django.contrib.auth.urls')),
	path('movegoal/<int:id>/',views.move_goal,name='moveGoal'),
	path('addgoal/',views.add_goal,name='AddGoal'),
	path('home/',views.home,name='home'),
]

