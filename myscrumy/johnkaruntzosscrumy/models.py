from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class GoalStatus(models.Model):
	status_name = models.TextField(default = ".")
	def __str__(self):
		return self.status_name



class ScrumyGoals(models.Model):
	goal_name = models.TextField(default = ".")
	goal_id = models.IntegerField()
	created_by = models.TextField(default = ".")
	moved_by = models.TextField(default = ".")
	owner = models.TextField(default = ".")
	goal_status = models.ForeignKey(GoalStatus, on_delete=models.PROTECT,default = ".")
	user = models.ForeignKey(User, on_delete=models.CASCADE,default = ".")
	def __str__(self):
                return self.goal_name



class ScrumyHistory(models.Model):
	moved_by = models.TextField(default = ".")
	created_by = models.TextField(default = ".")
	moved_from = models.TextField(default = ".")
	moved_to = models.TextField(default = ".")
	time_of_action = models.TextField(default = ".")
	goal = models.ForeignKey(ScrumyGoals, on_delete=models.CASCADE,default = ".")
	def __str__(self):
                return self.created_by

