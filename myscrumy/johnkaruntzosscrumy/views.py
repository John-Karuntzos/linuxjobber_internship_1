from django.shortcuts import render
from django.http import HttpResponse
from johnkaruntzosscrumy.models import ScrumyGoals,GoalStatus,ScrumyHistory
from django.contrib.auth.models import User
import random

def index(request):
	all = ScrumyGoals.objects.all()
	output = ', '.join([g.goal_name for g in all])
	return HttpResponse(output)

def move_goal(request, id):
	context = {'error':'A record with that goal id does not exist'}
	if len(ScrumyGoals.objects.filter(goal_id=id)) == 0:
 		return render(request, 'johnkaruntzosscrumy/exception.html', context)
	else:
		return HttpResponse(ScrumyGoals.objects.get(goal_id=id))

def add_goal(request):
	rand = random.randrange(1000,9999)
	while len(ScrumyGoals.objects.filter(goal_id=rand)) > 0:
		rand = random.randrange(1000,9999)
	ScrumyGoals.objects.create(goal_name='Keep Learning Django', goal_id=rand, created_by='Louis', moved_by='Louis', 
	owner='Louis', goal_status=GoalStatus.objects.get(status_name='Weekly Goal'), user=User.objects.get(username='louis'))
	return HttpResponse('Added goal with id: %i' % rand)	

def home(request):
	context = {'all_users':User.objects.all(),'weekly_goals':ScrumyGoals.objects.filter(goal_status=1),
'daily_goals':ScrumyGoals.objects.filter(goal_status=2),'verify_goals':ScrumyGoals.objects.filter(goal_status=3),
'done_goals':ScrumyGoals.objects.filter(goal_status=4)}
	return render(request, 'johnkaruntzosscrumy/home.html', context)



# Create your views here.
