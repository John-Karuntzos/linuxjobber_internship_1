from django.contrib import admin
from . models import GoalStatus
from . models import ScrumyHistory
from . models import ScrumyGoals

# Register your models here.

admin.site.register(GoalStatus)
admin.site.register(ScrumyHistory)
admin.site.register(ScrumyGoals)
